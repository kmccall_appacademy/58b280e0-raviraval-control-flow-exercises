# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  res_str = ""
  str.each_char do |ch|
    next if ch == ch.downcase
    res_str += ch
  end
  res_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
    str[(str.length / 2) - 1..(str.length / 2)]
  else
    str[(str.length / 2)]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count(VOWELS.join)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1...num).each { |i| num *= i }
  num
end


# MEDIUM
# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  res = ""
  arr.each_with_index do |el, idx|
    unless idx == arr.length - 1
      res += el
      res += separator
    else
      res += el
    end
  end
  res
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  res = ""
  str.split("").each_with_index do |el, idx|
    if idx.even? #if odd
      res += el.downcase
    else
      res += el.upcase
    end
  end
  res
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str = str.split.map do |word|
    if word.length >= 5
      word.reverse
    else
      word
    end
  end
  str.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).map do |num|
    if num % 3 == 0 && num % 5 == 0
      "fizzbuzz"
    elsif num % 3 == 0
      "fizz"
    elsif num % 5 == 0
      "buzz"
    else
      num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr) #I assume I can't use the reverse method?
  res_array = []
  arr.each do |el|
    res_array.unshift(el)
  end
  res_array
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2...num).none? { |i| num % i == 0}
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  res = []
  (1..num).to_a.each do |i|
    res << i if num % i == 0
  end
  res.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |i| prime?(i) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_count = 0
  even_count = 0
  arr.each do |el|
    odd_count += 1 if el.odd?
    even_count += 1 if el.even?
  end
  if odd_count > 1
    arr.each do |el|
      return el if el.even?
    end
  else
    arr.each do |el|
      return el if el.odd?
    end
  end
end
